# PHP API
This package provides some methods to generate standardized API responses. It handles models, collection, pagination (with [Fractal](https://github.com/thephpleague/fractal)) and (validation) errors.

It also provides a reasonably sophisticated permission system built on top of the [Cartalyst Sentinel](https://cartalyst.com/manual/sentinel/2.0) package.

This package is integrated in our [devstack](https://bitbucket.org/codeyellow/devstack) repo.

This package needs some more work;

- API methods are a mess.
- More documentation.

# Installation
You need Laravel `5.*`. Install with Composer:

```json
"repositories": [
    {
        "type": "vcs",
        "url":  "ssh://git@bitbucket.org/codeyellow/php-api"
    }
],
"require": {
    "codeyellow/api": "0.1.0"
}
```

Then, in `config/app.php`, add the following to the `providers` array:

```php
'CodeYellow\Api\ApiServiceProvider',
```

# Usage
Extend a controller with `\CodeYellow\Api\Api`.

```php
<?php
class User extends \CodeYellow\Api\Api
{
    //
}
```

## Model
```php
<?php
$mUser = $this->service->findById($id);

if (!$mUser) {
    return $this->errorNotFound();
}

return $this->respondWithItem($mUser, new Transformer());
```

## Collection

### Collection with pagination
```php
<?php
$params = Input::get();
$paginator = new Paginator($this->service->findAllWithPaginator($params));

return $this->respondWithPaginator($paginator, new Transformer());
```

### Collection without pagination
```php
<?php
$params = Input::get();
$collection = $this->service->findAll($params);

return $this->respondWithCollection($collection, new Transformer());
```

## Validation errors
In your controller:

```php
<?php
$params = Input::get();

try {
    $mUser = $this->service->create($params);
    $this->setStatusCode(201);

    return $this->getModel($mUser->getId());
} catch (\CodeYellow\Api\Validator\Exception $e) {
    return $this->errorWrongArgs($e);
}
```

In your service:

```php
<?php
public function create(array $params = array())
{
    if ($params = $this->validator->parse($params)) {
        // Add user logic.
    }
}
```

In your validator, extend `\CodeYellow\Api\Validation\Validator`, like so:

```php
<?php
namespace App\User;

class Validator extends \CodeYellow\Api\Validation\Validator
{
    protected $rules = [
        'username' => 'required|string|unique:users',
    ];
}
```

## Permissions

There are two middleware classes that provide permission control.
Before you start using it, you'll need to add these to your
`Http\Kernel` class definition to make Laravel aware of them:

```php
<?php
class Kernel extends HttpKernel
{
    // ....
    protected $routeMiddleware = [
        'basicAccess' => 'CodeYellow\Api\Middleware\BasicAccess',
        'scopedAccess' => 'CodeYellow\Api\Middleware\scopedAccess',
    ];
}

```

### BasicAccess

This first middleware class simply provides gated access based on a
raw *semantically named* permission.  For example, you could have
something like this in your `routes.php`:

```php
<?php
Route::group(['middleware' => 'basicAccess'], function () {
    Route::get('customer/{id}', ['uses' => 'Customer@getModel', 'permission' => 'customer/view']);
    Route::post('customer/{id}', ['uses' => 'Customer@postModel', 'permission' => 'customer/edit']);
});
```

Now, customers can only be viewed if the user is logged in and has the
`customer/view` permission, and edited when the user (or their group)
has the `customer/edit` permission.  Note that the slash in the name
suggests a hierarchy, but this is (currently) unused.

### ScopedAccess

This middleware class extends on the `BasicAccess` class by adding a
mandatory second and optional third level to the permission checking.

Using the example above, it would be logical if customers would be
able to view their own information.  But with `BasicAccess` they can
either access the route, or they can't, but there is no finegrained
row-level permission checking.  This is where `ScopedAccess` comes in.

The `routes.php` is the same, except it will use a different
middleware:

```php
<?php
Route::group(['middleware' => 'scopedAccess'], function () {
    Route::get('customer/{id}', ['uses' => 'Customer@getModel', 'permission' => 'customer/view']);
    Route::post('customer/{id}', ['uses' => 'Customer@postModel', 'permission' => 'customer/edit']);
});
```

Now, in your controller (which extends from `CodeYellow\Api\Api`), put
the implementation:

```php
<?php
class Customer extends \CodeYellow\Api\Api
{
    function getModel($id = 0)
    {
        $mCustomer = $this->scoped([
            'all' => function () use ($id) {
                return $this->service->findById($id);
            },
            'own' => function () use ($id) {
	        $mCustomer = $this->getCurrentCustomer();
                return ($mCustomer->getId() == $id) ? $mCustomer : NULL;
            },
        ]);
        // ...  perform operations on the customer, as needed ...
        return $this->respondWithItem($mCustomer, new Transformer());
    }
}
```

Now, if the user has the permission `customer/view.all`, the first
closure is invoked and it will simply fetch the customer using the
service.  If the user has the permission `customer/view.own`, the
second closure is invoked and it can only ever return the customer's
own model object (but only if the ID matches).

Please note that order is important here!  The first scope in the list
for which the user has a matching permission will be selected.  That
means that usually, you'll want to list the scopes from "high" to
"low" permissions.  If someone happens to have both, this will select
both these permissions.


#### Transformer access

You can go one level deeper with `ScopedAccess`, and apply access
control on the field level.  Sometimes you may have some fields that
should only be visible to some users, and not to others.  For example,
let's say the customer has an attached "comments" field which is
filled in by the owner of the application.  This is internal stuff and
should probably not be accessible to the customer.  So, we shield it
by using a transformer which defined which fields to access.

Continuing with our previous example, the `getModel` method's return
statement would be the only thing we change, and we list the
permission classifiers in a call to the `transformed` helper method:

```php
<?php
class Customer extends \CodeYellow\Api\Api
{
    function getModel($id = 0)
    {
        /// .... fetch customer and check scope (see above) ....
	$tCustomer =  $this->transformed([
            'full' => new FullCustomerTransformer(),
            'restricted' => new RestrictedCustomerTransformer(),
            'minimal' => new MinimalCustomerTransformer(),
	    // Also allowed: returning a callable that expects an object
        ]);
        return $this->respondWithItem($mCustomer, $tCustomer);
    }
}
```

Then, we would give customers the permission
`customer/view.own:restricted` and admins the permission
`customer/view.all:full`.  Now, in this particular case one could
argue that this extra level is unnecessary and we could decide on the
secondary level alone.  However, we might decide to extend the system
so that customers can see minimal information about other customers,
with a matching transformer.  This could then be implemented by giving
customers *both* the permissions `customer/view.all:minimal` and
`customer/view.own:restricted`.

The classifier is linked to the previously selected scope, so if the
customer is viewing an object based on the `all` scope, they would get
the `minimal` permission for said object, while they would see the
`restricted` permission if the object was returned by the `own` scope.


#### Applying transformers at the object level

In the above example, if the client views their own details, because
of the ordering requirement, this code will actually trigger the
`customer/view.all:minimal' permission rather than the desired
`customer/view.own:restricted' one.  The 'all' permission triggers
first, and this restricts the transformer from returning only
`minimal', even though the object itself will be one they have more
rights on.

This is a general problem of set membership, and will also happen when
returning a collection of objects.  To solve this, you can add a
"membership decider" to the `scoped' method call:

```php
<?php
class Customer extends \CodeYellow\Api\Api
{
    function getModel($id = 0)
    {
        $mCustomer = $this->scoped([
            'all' => ['load' => function () use ($id) {
                return $this->service->findById($id);
            }, 'isMember' => function ($obj) {
	        return true;  // Everything is member of all
	    }],
            'own' => ['load' => function () use ($id) {
	        $mCustomer = $this->getCurrentCustomer();
                return ($mCustomer->getId() == $id) ? $mCustomer : NULL;
            }, 'isMember' => function($obj) {
	        $mCustomer = $this->getCurrentCustomer();
	        return ($mCustomer->getId() == $obj->getId());
	    }],
        ]);
        // ...  perform operations on the customer, as needed ...
	$tCustomer =  $this->transformed([ ... ]);
        return $this->respondWithItem($mCustomer, $tCustomer);
    }
}
```

The `isMember' checks will check whether the provided object is a
member of the set.  The transformer returned by `transformed' will be
a wrapper closure which first checks membership on every object in a
collection before calling the matching transformer that you've
defined by your own closures.


## Validator
`\CodeYellow\Api\Validation\Validator` is made in such a way that it does not need a factory to create. This allows for extending the validator class. For this to happen two methods are added:

 _verify_

```php
<?php
    /**
     * Validates $params against the predefined rules.
     *
     * @param array $params Parameters to be verified
     * @return boolean Is $params valid
     */
    public function verify(array $params)
```

This method returns true/false depending on if the parameters are ok. If verify is not ok, you can get the messages that are generated from the ->getMessageBag() method. This will return a messageBag with all the messages of validation errors.


 _parse_

```php
<?php
    /**
     * Parses $params to satisfy the validator.
     *
     * @param &$params Parameter to be parsed
     * @throws \CodeYellow\Api\Validation\Exception
     * @return $params after validation
     */
    public function parse(array &$params)
```

This method will throw an error if the parameters do not match the given rules. Furthermore, this method will parse any parameters in the right format. E.g. Assume we have the rule that 'age' is 'numeric'. Now 

```php
<?php
$params = ['age' => '10'];
var_dump($params['age'] === 10); // False
$validator->parse($params);
var_dump($params['age'] === 10); // True
```

## ArrayValidator
`\CodeYellow\Api\Validator\Validator\ArrayValidator` is created to do fast and easy validations on specific keys in arrays, such that no new validator needs to created for this. It provides only a limited set of options. For more complicated validators you need to create your own validator. This class is especially handy in validating parameters that are given for example to a search.

_verify_

```php
<?php
    /**
     * Verifies that a key in params satisfies the given rules
     *
     * @param array $params The array that might contain the key.
     * @param string $key The key that we search for.
     * @param string $rules The rules that the key needs to satisfy.
     * @param Illuminate\Support\MessageBag $messages MessageBag to
     * which messages are added if the verify fails.
     * @return boolean Does $key in $param satisfy $rules?
     */
    public function verify(array $params, $key, $rules, MessageBag &$messages = null)
```

This method will verify that the key $key in the array $params satisfies the rules $rules. It will add any messages if $key is not valid to the messagebag $messages. E.g.

```php
<?php
$messages = null;
$isValid = $arrayvalidator->verify(['foo' => 'bar'], 'foo', 'numeric', $messages);
var_dump($isValid); // False
var_dump($messages); // MessageBag with one message about how foo is not numeric
```

_get_

```php
<?php
    /**
     * Returns and parses a value from an array, if it satisfies the rules
     * throws an exception if they key does not abide to the rules.
     *
     * N.b. messagebag not needed here, since it can be fetched from the thrown
     * exception if applicable.
     *
     * @param array $params The array that might contain the key.
     * @param string $key The key that we search for.
     * @param string $rules The rules that the key needs to satisfy.
     * @throws \CodeYellow\Api\Validation\Exception
     * @return string $params[$key] parsed.
     */
    public function get(array $params, $key, $rules)
```

Gets key $key from array $params if it satisfies $rules, otherwise it throws an Exception. The Exception contains the validator object, which contains messages about what went wrong. 

E.g.
```php
<?php
$foo = $arrayValidator->get(['foo' => '1'], 'foo', 'integer');
var_dump($foo); // 1

$bar = $arrayValidator->get(['bar' => 'foo'], 'bar', 'integer');
// Throws \CodeYellow\Api\Validator\Exception
```


## Nested validation
The validator is able to do nested validation by using other validators. There are two sorts of nested validators. There is a validator for a nested object, where a key references to another object. Also there is a nested collection, where a key refers to a collection of objects that have to be evaluated with another evaluator.

First of all, other validators that are used need to be registered in the validator. This is done as follows:

```
class FooValidator extends \CodeYellow\Api\Validation\Validator
{
    protected $validators = [
        'bar' => 'CodeYellow\Api\Test\Validation\Mock\BarValidator'
    ];
```
Now we can reference the 'bar' validator when specifying rules

__nested validator__

The nested validator can be applied by adding 'nested:validator\_name' in the rules. So for example:

```
    protected $rules = [
        'nested' => 'required|array|nested:bar'
    ];
```

The bar validator will now be called on the value of the nested key


__nested collection validator__

The nested collection is almost the same as the nested validator. The only difference is that in the rules 'nested\_collection:validator\_name' needs to be used.

E.g.
```
    protected $rules = [
        'nested' => 'array|nested_collection:bar'
    ];
```


_