<?php
namespace CodeYellow\Api\Test;

use Mockery;

use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected static $faker = null;

    public function tearDown()
    {
        Mockery::close();
    }


    public static $cachedApp = null;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        if (static::$cachedApp === null) {

            $app = new \Illuminate\Foundation\Application(
                realpath(__DIR__.'/../')
            );

            $app->singleton(
                \Illuminate\Contracts\Console\Kernel::class,
                \Illuminate\Foundation\Console\Kernel::class
            );
            $app->singleton(
                \Illuminate\Contracts\Debug\ExceptionHandler::class,
                \Illuminate\Foundation\Exceptions\Handler::class
            );


            $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

            $app->singleton('CodeYellow\KeenMapi\MapiConnectInterface', function () {
                return new KeenMapi('test', 'test', 'test');
            });

            static::$cachedApp = $app;
        }

        return static::$cachedApp;
    }

    /**
     * Formats the response json to php.
     *
     * @return  array
     */
    public function responseJson($response = null)
    {
        if (!$response) {
            $response = $this->response;
        }
        return json_decode($response->getContent(), true);
    }

    public function getFaker()
    {
        if (static::$faker == null) {
            static::$faker = \Faker\Factory::create();
        }

        return static::$faker;
    }

    /**
     * Returns a translator mock that can be passed to test the validator
     * returns a random string for each validation
     */
    protected function getTranslator()
    {
        $translator = \Mockery::mock('\Illuminate\Translation\Translator');
        $translator->shouldReceive('trans')
            ->zeroOrMoreTimes()
            ->andReturn('');
        return $translator;
    }

    /**
     * Returns a PresenceVerifierInstance
     *
     * @param int $returnValue What should count return?
     */
    protected function getPresenceVerifier($returnValue = 1)
    {
        $verifier = \Mockery::mock('\Illuminate\Validation\PresenceVerifierInterface');
        $verifier->shouldReceive('getCount')
            ->zeroOrMoreTimes()
            ->andReturn($returnValue);

        $verifier->shouldReceive('getMultiCount')
            ->zeroOrMoreTimes()
            ->andReturn($returnValue);

        $verifier->shouldReceive('setConnection')
            ->zeroOrMoreTimes();

        return $verifier;
    }
}
