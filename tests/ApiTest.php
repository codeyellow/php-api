<?php
namespace CodeYellow\Api\Test;

use CodeYellow\Api\Fractal\Pagination\Paginator;
use CodeYellow\Api\Test\Validation\Mock\ApiValidator;
use Mockery;

/**
 * @group api
 */
class ApiTest extends TestCase
{
    protected $manager = null;
    protected $api = null;

    public function setUp()
    {
        parent::setUp();
        $this->api = $this->app->make(\CodeYellow\Api\Test\Api::class);
        $this->request = $this->app->make(\Illuminate\Http\Request::class);
    }

    /**
     * Test that setting and getting a status code works correctly.
     */
    public function testStatusCode()
    {
        $this->api->setStatusCode(400);

        $status = $this->api->getStatusCode();

        $this->assertSame(400, $status);

        $this->response = $this->api->respondWithArray([]);

        $this->assertResponseStatus(400);
        $this->assertSame('[]', $this->response->getContent());
    }

    /**
     * Test that respondWithArray() works.
     */
    public function testRespondWithArray()
    {
        $options = [
            ['Code' => 'Yellow'],
            ['Or' => 'acle']
        ];

        $this->response = $this->api->respondWithArray($options);

        $this->assertResponseStatus(200);

        $json = $this->responseJson();
        $this->assertSame($options, $json);

        // Test that an empty HTTP_ACCEPT header returns json.
//        Input::shouldReceive('server')->andReturn('*/*')->shouldReceive('setUserResolver');

        $this->response = $this->api->respondWithArray($options);

        $this->assertResponseStatus(200);

        $json = $this->responseJson();
        $this->assertSame($options, $json);
    }

//    /**
//     * Test that respondWithArray() can return yaml.
//     */
//    public function testRespondWithArrayYaml()
//    {
//        $options = [
//            ['Code' => 'Yellow'],
//            ['Or' => 'acle']
//        ];
//
////        Input::shouldReceive('server')->andReturn('application/x-yaml');
//
//        $this->response = $this->api->respondWithArray($options);
//
//        $this->assertResponseStatus(200);
//
//        $parser = new YamlParser();
//
//        $yaml = $parser->parse($this->response->getContent());
//
//        $this->assertSame($options, $yaml);
//    }

    /**
     * Test that errorCsrf works
     * @group test
     */
    public function testErrorCsrf()
    {
        $message = 'Please dont do this.';

        $this->response = $this->api->errorCsrf($message);

        $this->assertResponseStatus(403);

        $json = $this->responseJson();

        $expectedJson = [
            'error' => [
                'code' => 'CsrfTokenInvalid',
                'http_code' => 403,
                'message' => $message
            ]
        ];

        $this->assertSame($expectedJson, $json);
    }

    /**
     * Test that errorForbidden() works.
     * @group test
     */
    public function testErrorForbidden()
    {
        $message = 'Please dont do this.';

        $this->response = $this->api->errorForbidden($message);

        $this->assertResponseStatus(403);

        $json = $this->responseJson();

        $expectedJson = [
            'error' => [
                'code' => 'InsufficientAccountPermissions',
                'http_code' => 403,
                'message' => $message
            ]
        ];

        $this->assertSame($expectedJson, $json);
    }

    /**
     * Test that errorInternalError() works.
     */
    public function testErrorInternalError()
    {
        $message = 'We have reason to believe the server has been stupid.';

        $this->response = $this->api->errorInternalError($message);

        $this->assertResponseStatus(500);

        $json = $this->responseJson();

        $expectedJson = [
            'error' => [
                'code' => 'InternalError',
                'http_code' => 500,
                'message' => $message
            ]
        ];

        $this->assertSame($expectedJson, $json);
    }

    /**
     * Test that errorNotFound() works.
     */
    public function testErrorNotFound()
    {
        $message = 'The package could not be found.';

        $this->response = $this->api->errorNotFound($message);

        $this->assertResponseStatus(404);

        $json = $this->responseJson();

        $expectedJson = [
            'error' => [
                'code' => 'ResourceNotFound',
                'http_code' => 404,
                'message' => $message
            ]
        ];

        $this->assertSame($expectedJson, $json);
    }

    /**
     * Test that errorUnauthorized() works.
     */
    public function testErrorUnauthorized()
    {
        $message = 'Not good enough.';

        $this->response = $this->api->errorUnauthorized($message);

        $this->assertResponseStatus(401);

        $json = $this->responseJson();

        $expectedJson = [
            'error' => [
                'code' => 'AuthenticationFailed',
                'http_code' => 401,
                'message' => $message
            ]
        ];

        $this->assertSame($expectedJson, $json);
    }

    /**
     * Test that respondWithError() throws an exception when
     * used with statuscode 200.
     *
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidArgumentException200()
    {
        $this->api->setStatusCode(200);
        $this->response = $this->api->respondWithError('No.', '');
    }

    /**
     * Test that respondWithError() gives an exception when
     * used with a 200 status code.
     *
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidArgumentExceptionOn200()
    {
        $errors = ['serious' => 'shit'];

        $this->api->setStatusCode(200);
        $this->response = $this->api->respondWithError('No.', '', $errors);
    }

    /**
     * Test that respondWithCollection() works.
     */
    public function testRespondWithCollection()
    {
        // Create some random data.
        $faker = $this->getFaker();
        $data = [];
        foreach (range(1, 5) as $item) {
            $data[] = $this->fakeItem();
        }

        $countData = count($data);

        // Mock transformer.
        $transformer = Mockery::mock('League\Fractal\TransformerAbstract')->makePartial();
        $transformer->shouldReceive('transform')->times($countData * 2)->andReturnUsing(function (array $data) {
            return ['name' => $data['name']];
        });

        // Create a Laravel collection.
        $collection = collect($data);

//        $this->request->shouldReceive('get')->andReturn([]);
        $this->response = $this->api->respondWithCollection($collection, $transformer);
        $this->assertResponseStatus(200);

        $json = $this->responseJson();

        $this->assertCount(5, $json['data']);
        // Test one item.
        $this->assertSame($data[0]['name'], $json['data'][0]['name']);
        $this->assertArrayNotHasKey('address', $json['data'][0]);

        // Test empty collection.
        $this->response = $this->api->respondWithCollection(collect([]), $transformer);

        $json = $this->responseJson();

        $this->assertSame([], $json['data']);

        // Test that includes works.
        $this->mockTransformerInclude($transformer);

        $this->response = $this->api->respondWithCollection($collection, $transformer, 'address');

        $json = $this->responseJson();

        $this->assertCount($countData, $json['data']);
        // Test one item.
        $this->assertSame($data[0], $json['data'][0]);
    }

    public function fakeItem()
    {
        $faker = $this->getFaker();

        return [
            'name' => $faker->name,
            'address' => [
                'street' => $faker->streetAddress,
            ],
        ];
    }

    public function mockTransformerInclude($transformer)
    {
        $transformer->shouldReceive('getAvailableIncludes')->andReturn(['address']);
        $transformer->shouldReceive('includeAddress')->andReturnUsing(function (array $data) use ($transformer) {
            return $transformer->item($data['address'], function ($value) use ($transformer) {
                return [
                    'street' => $value['street'],
                ];
            });
        });
    }

    /**
     * Test that respondWithItem() works.
     */
    public function testRespondWithItem()
    {
        $data = $this->fakeItem();

        // Mock transformer.
        $transformer = Mockery::mock('League\Fractal\TransformerAbstract')->makePartial();
        $transformer->shouldReceive('transform')->twice()->andReturnUsing(function (array $data) {
            return ['name' => $data['name']];
        });

        $this->response = $this->api->respondWithItem($data, $transformer);

        $this->assertResponseStatus(200);

        $json = $this->responseJson();

        $this->assertCount(1, $json);
        $this->assertSame($data['name'], $json['name']);

        // Test that includes works.
        $this->mockTransformerInclude($transformer);

        $this->response = $this->api->respondWithItem($data, $transformer, 'address');

        $this->assertResponseStatus(200);

        $json = $this->responseJson();

        $this->assertCount(2, $json);
        $this->assertSame($data['name'], $json['name']);
        $this->assertSame($data['address']['street'], $json['address']['street']);
    }

    /**
     * Test that respondWithPaginator() works.
     */
    public function testRespondWithPaginator()
    {
        // Create some random data.
        $faker = $this->getFaker();
        $data = [];
        foreach (range(1, 30) as $item) {
            $data[] = $this->fakeItem();
        }

        $countData = count($data);

        // Mock LengthAwarePaginator.
        $paginator = Mockery::mock('Illuminate\Pagination\LengthAwarePaginator')->makePartial();
        $paginator->shouldReceive('getCollection')->andReturn(collect($data));
        $paginator->shouldReceive('total')->andReturn($countData);
        $paginator->shouldReceive('currentPage')->andReturn(2);

        $adapter = new Paginator($paginator);

        // Mock transformer.
        $transformer = Mockery::mock('League\Fractal\TransformerAbstract')->makePartial();
        $transformer->shouldReceive('transform')->times($countData * 2)->andReturnUsing(function (array $data) {
            return ['name' => $data['name']];
        });

        $this->response = $this->api->respondWithPaginator($adapter, $transformer);
        $this->assertResponseStatus(200);

        $json = $this->responseJson();

        $this->assertCount($countData, $json['data']);
        $this->assertSame($countData, $json['totalRecords']);
        // Test one item.
        $this->assertSame($data[0]['name'], $json['data'][0]['name']);
        $this->assertArrayNotHasKey('address', $json['data'][0]);

        // Test that includes works.
        $this->mockTransformerInclude($transformer);

        $this->response = $this->api->respondWithPaginator($adapter, $transformer, 'address');

        $json = $this->responseJson();

        $this->assertCount($countData, $json['data']);
        $this->assertSame($countData, $json['totalRecords']);
        // Test one item.
        $this->assertSame($data[0], $json['data'][0]);
    }

    /**
     * Test that errorWrongArgs() works.
     */
    public function testErrorWrongArgs()
    {

        $validator = new ApiValidator($this->getTranslator(), $this->getPresenceVerifier());

        $rules = ['test' => 'required|min:5|mimes:png'];
        $validator->setRules($rules);

        $params = ['test' => 'sd', 'extra' => 'hoi'];

        $this->response = null;

        try {
            $validator->parse($params);
        } catch (\Illuminate\Validation\ValidationException $e) {
            $this->response = $this->api->errorWrongArgs($e);
        }

        $this->assertNotNull($this->response);
        $this->assertResponseStatus(400);

        $json = $this->responseJson();

        $this->assertArrayHasKey('error', $json);
        $this->assertSame('ParameterError', $json['error']['code']);
        $this->assertSame(400, $json['error']['http_code']);
        $this->assertArrayHasKey('validation_errors', $json['error']);
        $this->assertCount(1, $json['error']['validation_errors']);
        $this->assertArrayHasKey('test', $json['error']['validation_errors']);

        $this->assertCount(2, $json['error']['validation_errors']['test']);
        $this->assertSame(['code' => 'min', 'min' => '5'], $json['error']['validation_errors']['test'][0]);
        $this->assertSame(['code' => 'mimes', 'values' => 'png'], $json['error']['validation_errors']['test'][1]);
    }
}
