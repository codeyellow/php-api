<?php

namespace CodeYellow\Api\Test\Middleware;

use Cartalyst\Sentinel\Permissions\PermissionsTrait;
use Cartalyst\Sentinel\Permissions\PermissionsInterface;

// From Sentinel's tests.  Nasty hackery to get actual permission
// objects used.  See the Middleware tests
class PermissionsStub implements PermissionsInterface
{
    use PermissionsTrait;

    protected function createPreparedPermissions()
    {
        $prepared = [];

        if (! empty($this->secondaryPermissions)) {
            foreach ($this->secondaryPermissions as $permissions) {
                $this->preparePermissions($prepared, $permissions);
            }
        }

        if (! empty($this->permissions)) {
            $permissions = [];
            $this->preparePermissions($permissions, $this->permissions);
            $prepared = array_merge($prepared, $permissions);
        }

        return $prepared;
    }
}
