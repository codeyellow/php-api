<?php
namespace CodeYellow\Api\Test\Middleware;

use CodeYellow\Api\Middleware\BasicAccess;

class ExceptionTestException extends \Exception{};

/**
 * @group middleware
 * @group basicAccess
 */
class BasicAccessTest extends \CodeYellow\Api\Test\Middleware\TestCase
{
    static $middlewareName = '\CodeYellow\Api\Middleware\BasicAccess';

    public function testRouteWithoutPermissionThrowsException()
    {
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');
        $this->runRoute([]);
    }

    public function testUnauthenticatedAccessDenied()
    {
        $this->anonymousUser();
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');
        $this->runRoute(['permission' => 'whatever-you-dont-have-it']);
    }

    public function testAuthenticatedWithoutPermissionAccessDenied()
    {
        $this->loginUserWithPermissions([]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        $this->runRoute(['permission' => 'some-permission']);
    }

    public function testAuthenticatedWithoutPermissionDueToExplicitlyFalseAccessDenied()
    {
        $this->loginUserWithPermissions(['some-permission' => false]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        $this->runRoute(['permission' => 'some-permission']);
    }

    public function testAuthenticatedWithPermissionAccessAllowed()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $this->assertSame('OK', $result->getOriginalContent());
    }

    public function testAuthenticatedWithMoreSpecificScopedPermissionAccessAllowed()
    {
        $this->loginUserWithPermissions(['some-permission.everything' => true]);

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $this->assertSame('OK', $result->getOriginalContent());
    }

    public function testAuthenticatedWithEvenMoreSpecificScopedAndClassifiedPermissionAccessAllowed()
    {
        $this->loginUserWithPermissions(['some-permission.everything:subview' => true]);

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $this->assertSame('OK', $result->getOriginalContent());
    }

    // We're using wildcards, so this tests that we aren't too lax
    public function testAuthenticatedWithPermissionDifferentSuffixAccessDenied()
    {
        $this->loginUserWithPermissions(['some-permission-that-doesnt-match' => true]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $this->assertSame('OK', $result->getOriginalContent());
    }

    public function testAuthenticatedWithPermissionPartialPrefixAccessDenied()
    {
        $this->loginUserWithPermissions(['some' => true]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $this->assertSame('OK', $result->getOriginalContent());
    }

    // Check that the implementation details don't unnecessarily leak out
    public function testActionIsCleanedUpAfterCompleting()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);

        list($router, $result) = $this->runRoute(['permission' => 'some-permission']);
        $route = $router->current(); // Not reset after completion: implementation detail?
        $this->assertEmpty(array_diff(array_keys($route->getAction()), ['uses', 'middleware', 'permission']));
    }

    public function testActionIsCleanedUpWhenExceptionThrown()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);

        // Can't use runRoute here
        $router = new \Illuminate\Routing\Router(new \Illuminate\Events\Dispatcher);
        $router->get('test', ['permission' => 'some-permission', 'uses' => function() {
                    throw new ExceptionTestException();
                }, 'middleware' => static::$middlewareName]);
        $this->currentRequest = \Illuminate\Http\Request::create('test', 'GET');

        $seenException = false;
        try {
            $result = $router->dispatch($this->currentRequest);
        } catch (ExceptionTestException $e) {
            $seenException = true;
        }
        $this->assertTrue($seenException);
        $route = $router->current(); // Not reset after completion: implementation detail?
        $this->assertEmpty(array_diff(array_keys($route->getAction()), ['uses', 'middleware', 'permission']));
    }
}
