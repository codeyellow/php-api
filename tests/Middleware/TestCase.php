<?php

namespace CodeYellow\Api\Test\Middleware;

use CodeYellow\Api\Sentinel\SentinelTestServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;

abstract class TestCase extends \CodeYellow\Api\Test\TestCase
{
    protected $currentRequest;

    public function setUp()
    {
        parent::setUp();
        // Ensure Sentinel is registered so we can access the persistences attributes and so on
        $this->app->make('sentinel');
    }

    protected function anonymousUser()
    {
        SentinelTestServiceProvider::$persistences->shouldReceive('check')->once()->andReturn(False);
    }

    /**
     * XXX TODO: This is fucking horrible: The Sentinel stuff is
     * _completely_ mocked out, so it isn't a realistic test of
     * whether the access check is done correctly.  This means
     * that the tests may pass with flying colours while the
     * application could crash and burn...
     *
     * Unfortunately, to mock out or use a realistic Sentinel object
     * requires a fully bootstrapped Laravel environment.  That's a
     * hoop I'm not willing to jump through.  What's really needed is
     * for Sentinel itself to provide a "null" implementation that can
     * be used in tests.  However, this implementation then would need
     * to be tested as well. Alternatively, Laravel could supply a
     * null in-memory "database", but that produces similar
     * problems. This is recursively infinite retardation.
     *
     * Anyway, this is a middle ground: we create a fake user object
     * that defers to a stubbed out but REAL permissions object.
     */
    protected function loginUserWithPermissions($permissions)
    {
        $perm = new PermissionsStub($permissions);
        $user = \Mockery::mock('Cartalyst\Sentinel\Users\EloquentUser')->shouldDeferMissing();
        $user->shouldReceive('hasAnyAccess')->andReturnUsing(function() use ($perm) {
                return call_user_func_array([$perm, 'hasAnyAccess'], func_get_args());
        });
        \App::make('sentinel')->setUser($user);
    }

    // Run a route (ignoring the name) with the supplied action array.
    // Returns the router, so we can inspect the routes or actions.
    protected function runRoute($action)
    {
        $router = new Router(new \Illuminate\Events\Dispatcher);
        $router->get('test', array_merge(['uses' => function() { return 'OK'; }, 'middleware' => static::$middlewareName], $action));
        $this->currentRequest = Request::create('test', 'GET');
        $result = $router->dispatch($this->currentRequest);
        return [$router, $result];
    }
}
