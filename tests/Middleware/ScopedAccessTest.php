<?php

use CodeYellow\Api\Middleware\ScopedAccess;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionTestException extends \Exception{};

/**
 * @group middleware
 * @group scopedAccess
 */
class ScopedAccessTest extends \CodeYellow\Api\Test\Middleware\TestCase
{
    static $middlewareName = '\CodeYellow\Api\Middleware\ScopedAccess';

    ///////////// Basic access and scope tests //////////

    public function testRouteWithoutPermissionThrowsException()
    {
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');
        $this->runRoute(['uses' => function() { throw new \Exception('Should not get here'); }]);
    }

    public function testUnauthenticatedAccessDenied()
    {
        $this->anonymousUser();
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');
        $this->runRoute(['permission' => 'whatever-you-dont-have-it',
                         'uses' => function() { throw new \Exception('Should not get here'); }]);
    }

    public function testAuthenticatedWithoutPermissionAccessDenied()
    {
        $this->loginUserWithPermissions([]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() { throw new \Exception('Should not get here'); }]);
    }

    public function testAuthenticatedWithoutPermissionDueToExplicitlyFalseAccessDenied()
    {
        $this->loginUserWithPermissions(['some-permission' => false]);
        $this->expectException('\Symfony\Component\HttpKernel\Exception\HttpException');

        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() { throw new \Exception('Should not get here'); }]);
    }

    public function testAuthenticatedWithPermissionButMissingCheckThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $this->runRoute(['permission' => 'some-permission']);
    }

    public function testAuthenticatedWithPermissionButMissingCheckThrowsNoExceptionIf500ErrorIsGenerated()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);
        $this->runRoute([
            'permission' => 'some-permission',
            'uses' => function () {
                return Response::make('fwig;awgh', 500);
            }
        ]);
        $this->assertTrue(true);
    }


    public function testAuthenticatedWithPermissionButMissingScopeThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                return  ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest,
                                    ['something-else' => function() { throw new \Exception('SCOPE SHOULD NOT BE APPLIED'); }]);
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }

    public function testAuthenticatedWithPermissionButUnkownScopeThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.nonexistent' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                return ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest,
                                    ['something-else' => function() { throw new \Exception('SCOPE SHOULD NOT BE APPLIED'); }]);
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }

    // Test for wildcard abuse, like in BasicAccess
    public function testAuthenticatedWithPermissionButScopeWithUnknownSuffixThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.foobar' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                return ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest,
                                    ['foo' => function() { throw new \Exception('SCOPE SHOULD NOT BE APPLIED'); }]);
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }

    public function testAuthenticatedWithPermissionAppliesMatchingScope()
    {
        $this->loginUserWithPermissions(['some-permission.scope2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['scope1' => function() { throw new \Exception('FIRST SCOPE'); },
                                                              'scope2' => function() { return 'SECOND SCOPE'; },
                                                              'scope3' => function() { throw new \Exception('THIRD SCOPE'); }]);
                                                     }]);
        $this->assertSame('SECOND SCOPE', $result->getOriginalContent());
    }

    public function testAuthenticatedWithPermissionAndClassifierAppliesMatchingScope()
    {
        $this->loginUserWithPermissions(['some-permission.scope2:some-classifier' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['scope1' => function() { throw new \Exception('FIRST SCOPE'); },
                                                              'scope2' => function() { return 'SECOND SCOPE'; },
                                                              'scope3' => function() { throw new \Exception('THIRD SCOPE'); }]);
                                                     }]);
        $this->assertSame('SECOND SCOPE', $result->getOriginalContent());
    }

    public function testAuthenticatedWithPermissionAppliesOnlyFirstMatchingScope()
    {
        $this->loginUserWithPermissions(['some-permission.scope1' => true, 'some-permission.scope2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['scope1' => function() { return 'FIRST SCOPE'; },
                                                              'scope2' => function() { throw new \Exception('SECOND SCOPE'); },
                                                              'scope3' => function() { throw new \Exception('THIRD SCOPE'); }]);
                                                     }]);
        $this->assertSame('FIRST SCOPE', $result->getOriginalContent());
    }

    public function testAuthenticatedWithPermissionAppliesOnlyFirstTrueMatchingScope()
    {
        $this->loginUserWithPermissions(['some-permission.scope1' => false, 'some-permission.scope2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['scope1' => function() { throw new \Exception('FIRST SCOPE'); },
                                                              'scope2' => function() { return 'SECOND SCOPE'; },
                                                              'scope3' => function() { throw new \Exception('THIRD SCOPE'); }]);
                                                     }]);
        $this->assertSame('SECOND SCOPE', $result->getOriginalContent());
    }

    // Check correct context is remembered: some-other-permission should be ignored
    public function testAuthenticatedWithPermissionAppliesFirstMatchingScopeForThatPermissionOnly()
    {
        $this->loginUserWithPermissions(['some-other-permission.scope1' => true, 'some-permission.scope2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['scope1' => function() { throw new \Exception('FIRST SCOPE'); },
                                                              'scope2' => function() { return 'SECOND SCOPE'; },
                                                              'scope3' => function() { throw new \Exception('THIRD SCOPE'); }]);
                                                     }]);
        $this->assertSame('SECOND SCOPE', $result->getOriginalContent());
    }

    ///////////// Classifier tests //////////

    public function testClassifierWithoutPrecedingScopeCheckThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.scope:classifier' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                    $test->currentRequest,
                                    ['classifier' => function() { return 'SHOULD NOT GET HERE'; }]);
                                return $trans('x');
                            }]);
    }

    public function testClassifierAuthenticatedWithPermissionAndScopeButMissingClassifierThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $result = $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                    $test->currentRequest, ['some-classifier' => function() { throw new \Exception('CLASSIFIER SHOULD NOT BE APPLIED'); }]);
                                $trans('x');
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }

    public function testClassifierAuthenticatedWithPermissionAndScopeButUnkownClassifierThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:some-classifier' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                    $test->currentRequest, ['some-other-classifier' => function() { throw new \Exception('CLASSIFIER SHOULD NOT BE APPLIED'); }]);
                                $trans('x');
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }


    // Test for wildcard abuse, like in BasicAccess and above with scope
    public function testClassifierAuthenticatedWithPermissionAndScopeButUnknownClassifierSuffixThrowsException()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:foobar' => true]);
        $this->expectException('\CodeYellow\Api\Middleware\InsecurityException');

        $test = $this;
        $this->runRoute(['permission' => 'some-permission',
                         'uses' => function() use ($test) {
                                ScopedAccess::applyScopeForMatchingPermission(
                                    $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                    $test->currentRequest, ['foo' => function() { throw new \Exception('CLASSIFIER SHOULD NOT BE APPLIED'); }]);
                                $trans('x');
                                throw new \Exception('SHOULD NOT GET HERE');
                         }]);
    }

    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesMatchingClassifier()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:classifier2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest,
                                                             ['classifier1' => function() { throw new \Exception('FIRST CLASSIFIER'); },
                                                              'classifier2' => function() { return 'SECOND CLASSIFIER'; },
                                                              'classifier3' => function() { throw new \Exception('THIRD CLASSIFIER'); }]);
                                                         return $trans('x');
                                                     }]);
        $this->assertSame('SECOND CLASSIFIER', $result->getOriginalContent());
    }

    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesMatchingClassifierAndCallsTransformerCorrectly()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:some-classifier' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {

                                                         $transformer = \Mockery::mock('League\Fractal\TransformerAbstract')->makePartial();
                                                         $transformer->shouldReceive('transform')->andReturn('TRANSFORMER OK');

                                                         ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest, ['some-classifier' => $transformer]);
                                                         return $trans('x');
                                                     }]);
        $this->assertSame('TRANSFORMER OK', $result->getOriginalContent());
    }

    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesOnlyFirstMatchingClassifier()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:classifier1' => true, 'some-permission.some-scope:classifier2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest,
                                                             ['classifier1' => function() { return 'FIRST CLASSIFIER'; },
                                                              'classifier2' => function() { throw new \Exception('SECOND CLASSIFIER'); },
                                                              'classifier3' => function() { throw new \Exception('THIRD CLASSIFIER'); }]);
                                                         return $trans('x');
                                                     }]);
        $this->assertSame('FIRST CLASSIFIER', $result->getOriginalContent());
    }

    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesOnlyFirstTrueMatchingClassifier()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope:classifier1' => false, 'some-permission.some-scope:classifier2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest,
                                                             ['classifier1' => function() { throw new \Exception('FIRST CLASSIFIER'); },
                                                              'classifier2' => function() { return 'SECOND CLASSIFIER'; },
                                                              'classifier3' => function() { throw new \Exception('THIRD CLASSIFIER'); }]);
                                                         return $trans('x');
                                                     }]);
        $this->assertSame('SECOND CLASSIFIER', $result->getOriginalContent());
    }

    // Check correct context is remembered: some-other-scope should be ignored
    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesFirstMatchingClassifierForThatScopeOnly()
    {
        $this->loginUserWithPermissions(['some-permission.some-other-scope:classifier1' => true, 'some-permission.some-scope:classifier2' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['some-scope' => function() { return 'OK'; }]);

                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest,
                                                             ['classifier1' => function() { throw new \Exception('FIRST CLASSIFIER'); },
                                                              'classifier2' => function() { return 'SECOND CLASSIFIER'; },
                                                              'classifier3' => function() { throw new \Exception('THIRD CLASSIFIER'); }]);
                                                         return $trans('x');
                                                     }]);
        $this->assertSame('SECOND CLASSIFIER', $result->getOriginalContent());
    }

    /////////////// Classifier with membership decider tests /////////////////////

    public function testClassifierAuthenticatedWithPermissionScopeAndClassifierAppliesMatchingClassifierForScopeThroughMembershipDecider()
    {
        $this->loginUserWithPermissions(['some-permission.large-scope:low-classifier' => true, 'some-permission.small-scope:high-classifier' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         $collection = ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest, ['large-scope' => ['load' => function() { return ['other1', 'other2', 'self']; },
                                                                                                       'isMember' => function() { return true; } ],
                                                                                     'small-scope' => ['load' => function() { throw new \Exception('Should load high scope!'); },
                                                                                                       'isMember' => function($x) { return $x == 'self'; } ]]);

                                                         $high = []; $low = [];
                                                         $trans = ScopedAccess::applyClassifierForMatchingPermissionScope(
                                                             $test->currentRequest,
                                                             ['high-classifier' => function ($obj) use (&$high) { $high[] = $obj; },
                                                              'low-classifier' => function ($obj) use (&$low) { $low[] = $obj; },]);

                                                         // Manually walk the collection.  Normally this would be in the controller,
                                                         // calling respondwithcollection or some such.
                                                         array_walk($collection, $trans);
                                                         return 'high:'.implode($high, ',').'|low:'.implode($low, ',');
                                                     }]);
        $this->assertSame('high:self|low:other1,other2', $result->getOriginalContent());
    }

    // Check that the implementation details don't unnecessarily leak out
    public function testActionIsCleanedUpAfterCompleting()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope' => true]);

        $test = $this;
        list($router, $result) = $this->runRoute(['permission' => 'some-permission',
                                                  'uses' => function() use ($test) {
                                                         return ScopedAccess::applyScopeForMatchingPermission(
                                                             $test->currentRequest,
                                                             ['some-scope' => function() { return 'OK'; } ]);
                                                     }]);

        $route = $router->current(); // Not reset after completion: implementation detail?
        $this->assertEmpty(array_diff(array_keys($route->getAction()), ['uses', 'middleware', 'permission']));
    }

    public function testActionIsCleanedUpWhenExceptionThrown()
    {
        $this->loginUserWithPermissions(['some-permission.some-scope' => true]);

        // Can't use runRoute here
        $test = $this;
        $router = new \Illuminate\Routing\Router(new \Illuminate\Events\Dispatcher);
        $router->get('test', ['permission' => 'some-permission', 'uses' => function() use ($test) {
                    ScopedAccess::applyScopeForMatchingPermission(
                        $test->currentRequest,
                        ['some-scope' => function() { return 'OK'; } ]);
                    throw new ExceptionTestException();
                }, 'middleware' => static::$middlewareName]);
        $this->currentRequest = \Illuminate\Http\Request::create('test', 'GET');

        $seenException = false;
        try {
            $result = $router->dispatch($this->currentRequest);
        } catch (ExceptionTestException $e) {
            $seenException = true;
        }
        $this->assertTrue($seenException);
        $route = $router->current(); // Not reset after completion: implementation detail?
        $this->assertEmpty(array_diff(array_keys($route->getAction()), ['uses', 'middleware', 'permission']));
    }
}
