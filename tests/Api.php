<?php
namespace CodeYellow\Api\Test;

/**
 * Make a non abstract version of the api, such that we can initiate it
 * If we move to PHP7, change this with an anonymous class.
 */
class Api extends \CodeYellow\Api\Api
{
    
}
