<?php
namespace CodeYellow\Api\Fractal\Serializer;

use League\Fractal\Pagination\PaginatorInterface;

class BackboneSerializer extends \League\Fractal\Serializer\ArraySerializer
{
    /**
     * Serialize the meta.
     *
     * @param array $meta
     *
     * @return array
     **/
    public function meta(array $meta)
    {
        if (empty($meta)) {
            return array();
        }

        return $meta;
    }

    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     **/
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey === null) {
            return $data;
        }

        return [$resourceKey ?: 'data' => $data];
    }

    /**
     * Serialize the paginator.
     *
     * @param \League\Fractal\Pagination\PaginatorInterface $paginator
     *
     * @return array
     **/
    public function paginator(PaginatorInterface $paginator)
    {
        return [
            'totalRecords' => (int) $paginator->getTotal(),
        ];
    }
}
