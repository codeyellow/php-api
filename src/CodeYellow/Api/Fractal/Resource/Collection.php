<?php
namespace CodeYellow\Api\Fractal\Resource;

class Collection extends \League\Fractal\Resource\Collection
{
    /**
     * Create a new resource instance.
     *
     * @param mixed           $data
     * @param callable|string $transformer
     * @param string          $resourceKey
     */
    public function __construct($data, $transformer, $resourceKey = 'data')
    {
        parent::__construct($data, $transformer, $resourceKey);
    }
}
