<?php

namespace CodeYellow\Api\Middleware;

/**
 * This exception is thrown whenever an insecurity is found in the way
 * the permissions are checked.  This is usually either an
 * inconsistency in the database (mismatched permissions in DB versus
 * what the code expects) or an oversight by the developer where he or
 * she forgot to check a permission.
 */
class InsecurityException extends \RuntimeException
{
}