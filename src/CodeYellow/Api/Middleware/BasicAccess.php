<?php

namespace CodeYellow\Api\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BasicAccess
{
    protected static $memoryName = '___BasicAccess_private_memory_store';

	public function handle($request, Closure $next)
	{
        $this->ensureBasicAccess($request);
        try {
            $response = $next($request);
            return $response;
        } finally {
            $this->cleanup($request);
        }
	}

    /**
     * Enforce that the currently logged in user has the requested
     * "basic" permission.  If no permission was requested, complain
     * loudly to the developer.
     */
    protected function ensureBasicAccess($request)
    {
        /* Even though Middleware is supposed to supplant Laravel 4-style filters,
         * it doesn't receive arguments like filters do.  But we can extract custom
         * attributes from the route according to
         * http://blog.elliothesp.co.uk/coding/passing-parameters-middleware-laravel-5/
         * In Laravel 5.1, Middleware will supposedly receive arguments once again,
         * so we might want to consider refactoring this when we switch.
         */


        $action = $request->route()->getAction();
        if (empty($action['permission'])) {
            throw new InsecurityException('You have forgotten to define a basic permission for the route for URL '.$request->url().' (method '.$request->method().').  Shame on you!');
        }

        $permission = $action['permission'];
        $user = Sentinel::getUser();


        // The basic permission must either match exactly, or the
        // user needs to have a more specific scoped permission
        // below the named permissions.
        if (!$user) {
            // Same as App::abort(401), but works even if the app
            // isn't bootstrapped (which is a pain in the ass for tests)
            throw new HttpException(401, 'User is not logged in', null, []);
        } else if (!$user->hasAnyAccess([$permission, $permission.'.*'])) {

            // Same as App::abort(403), but works even if the app
            // isn't bootstrapped (which is a pain in the ass for tests)
            throw new HttpException(403, 'User does not have necessary basic permission ' . $permission, null, []);
        }
        static::rememberValue($request, 'appliedBasicPermission', $permission);
    }

    /**
     * Clean up remembered values.
     */
    protected function cleanup($request)
    {
        static::forgetValue($request, 'appliedBasicPermission');
    }

    /**
     * Remember a named value for this request.
     *
     * We attempt to manage our state so that the whole pipeline at
     * least _looks_ referentially transparent.  This makes it easier
     * to test, and less error-prone, hopefully.
     *
     * This is a bit of a hack, which is needed because middleware
     * cannot communicate at all with the routes or the controllers,
     * except via the request/response.  The API is designed to be
     * opaque to where the value is actually stored, so if we come up
     * with a cleaner way to do this, we can hopefully move it without
     * changing the callers.
     *
     * @param Illuminate/Http/Request $request The request for which to remember the value.
     * @param string $key The key under which to store the value.
     * @param mixed $value The value to remember.
     */
    protected static function rememberValue($request, $key, $value)
    {
        $action = $request->route()->getAction();
        assert(!isset($action[static::$memoryName][$key]));
        $action[static::$memoryName][$key] = $value;
        $request->route()->setAction($action);
    }

    /**
     * Forget a remembered value.
     *
     * @param Illuminate/Http/Request $request The request for which the value was remembered.
     * @param string $key The key under which the value was stored.
     */
    protected static function forgetValue($request, $key)
    {
        $action = $request->route()->getAction();
        assert(isset($action[static::$memoryName][$key]));
        unset($action[static::$memoryName][$key]);

        if (empty($action[static::$memoryName])) {
            unset($action[static::$memoryName]);
        }
        $request->route()->setAction($action);
    }

    /**
     * Retrieve a remembered value.
     *
     * @param Illuminate/Http/Request $request The request for which the value was remembered.
     * @param string $key The key under which the value was stored.
     * @return mixed The remembered value.
     */
    protected static function retrieveValue($request, $key)
    {
        $action = $request->route()->getAction();
        return $action[static::$memoryName][$key];
    }

    /**
     * Check if we have a value remembered.
     *
     * @param Illuminate/Http/Request $request The request for which the value was remembered.
     * @param string $key The key under which the value was stored.
     * @return bool True if there is a value for the request under this key.
     */
    protected static function haveRememberedValue($request, $key)
    {
        $action = $request->route()->getAction();
        return isset($action[static::$memoryName][$key]);
    }
}
