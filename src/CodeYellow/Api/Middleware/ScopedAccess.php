<?php

namespace CodeYellow\Api\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

/**
 * Scoped access offers a fine-grained access level below basic permission.
 *
 * For example, some user may have 'user/edit' as basic access to
 * allow editing users.  However, perhaps the user is only allowed to
 * edit users within the user's own company.  We can define a level
 * below that, like 'user/edit.company' to define that we can only
 * edit users below our own company.  An admin may have
 * 'user/edit.all' to define that the admin can edit all users.
 * Regular users will have 'user/edit.own' to define they can only
 * edit their own information.
 *
 * This middleware will enforce a check on the scoped permission,
 * raising an exception if at the end of an action no scope has been
 * requested so far.  This should *NOT* be relied upon for security
 * because the check will be done after all (possibly destructive)
 * controller actions have been performed, but it is simply a basic
 * sanity check to help you remember to scope the access.
 *
 * Finally, there's an optional third level below the scope, which is
 * the transformation classifier.  This can be used to determine for
 * each record which fields should be accessible.  For example, in the
 * example above, an admin may not see the other user's phone number
 * because that's private.  So we could define
 * 'user/edit.all:public-info' for admins and
 * 'user/edit.own:private-info' for users.  This is not enforced
 * because this is needed only about half the time.
 */
class ScopedAccess extends BasicAccess
{
	public function handle($request, Closure $next)
	{
        try {
            $response = parent::handle($request, $next);

            if (! ($response->getStatusCode() >= 500 && $response->getStatusCode() < 600)) {
                $this->ensureScopeWasChecked($request, $response);
            }
            return $response;
        } finally {
            if (static::haveRememberedValue($request, 'applicablePermissionScopes')) {
                static::forgetValue($request, 'applicablePermissionScopes');
            }
        }
	}

    /**
     * Enforce that a scope was requested.  This does not prevent a
     * dangerous action from being performed at all(!), but it will
     * prevent the action from returning a successful response, so
     * this will be caught during testing.
     */
    protected function ensureScopeWasChecked($request, $response)
    {
        if (!static::haveRememberedValue($request, 'applicablePermissionScopes')) {
            throw new InsecurityException('You have forgotten to check the secondary permission level.  Remember to call scoped()!');
        } else {
            static::forgetValue($request, 'applicablePermissionScopes');
        }
    }

    /**
     * Get the current scope for which this operation needs to be
     * applied, and apply the matching closure.
     *
     * Example: $collection = ScopedAccess::applyScopeForMatchingPermission($request, ['all' => function() use ($this) { return $this->service->findAll(); }]);
     *
     * @TODO: This could use some work: it's probably better to extend
     * Sentry to support searching for structured permission prefixes
     * and such.  It's a bit too hacky and redundant to have to
     * specify all the known scopes. (XXX: On second thought, is it
     * really?  It makes sense to specify them in one place)
     *
     * @TODO: Finally, it might make more sense to move this upwards
     * into the routes.  That way, you get a different method in the
     * controller for each access level, just like you get a different
     * method in the controller for each HTTP verb.
     *
     * @param Request $request The current request
     *
     * @param array $knownScopes The scopes that this method
     * recognises are keys in this array, in descending order of
     * permission level, with a closure object as value.
     * Alternatively, the value is an array containing 'load' and
     * 'isMember' keys, both of which have closure objects as value.
     *
     * @return mixed The value returned by the closure whose
     * key matches the scope name of the current user's permission.
     * If the user has none of the named scopes, there's a real
     * problem and an exception is thrown.
     */
    public static function applyScopeForMatchingPermission($request, $knownScopes)
    {
        $user = Sentinel::getUser();
        $basicPermission = static::retrieveValue($request, 'appliedBasicPermission');

        $scopes = static::normalizeAndFilterScopes($user, $basicPermission, $knownScopes);


        // Just apply the first one, since they're ranked from high to low
        foreach($scopes as $scopeName => $scope) {
            static::rememberValue($request, 'applicablePermissionScopes', $scopes);
            return $scope['loader']();
        }
        throw new InsecurityException('User has basic permission "'.$basicPermission.'", but none of the known scopes!');
    }

    /**
     * Helper method to normalize scopes for a given basic permission,
     * so they fit a common format, and filter out the scopes that are
     * don't apply to the user.
     */
    private static function normalizeAndFilterScopes($user, $basicPermission, $scopes)
    {
        $filteredScopes = [];

        foreach($scopes as $scopeName => $funcOrArray) {
            if ($user->hasAnyAccess(["{$basicPermission}.{$scopeName}", "{$basicPermission}.{$scopeName}:*"])) {
                if (is_callable($funcOrArray)) {
                    $loader = $funcOrArray;
                    $decider = function($obj) { return true; };
                } else if (!is_callable($funcOrArray)) {
                    if (!is_array($funcOrArray)) {
                        throw new InsecurityException('Scope "'.$scopeName.'" must be callable or an array');
                    } else if (!isset($funcOrArray['load'])) {
                        throw new InsecurityException('Scope "'.$scopeName.'" must have a "load" key');
                    } else {
                        $loader = $funcOrArray['load'];
                        $decider = isset($funcOrArray['isMember']) ? $funcOrArray['isMember'] : function($obj) { return true; };
                    }
                }
                $filteredScopes[$scopeName] = ['loader' => $loader, 'decider' => $decider];
            }
        }
        return $filteredScopes;
    }

    /**
     * Get the transformation classifier which this operation should
     * return, and apply the matching closure.  Can only be used after
     * calling scoped(), because it needs to know which of the
     * available permission scopes has been applied.
     *
     * Example: $transformer = ScopedAccess::applyScopeForMatchingPermission($request, ['full' => function() { return new DefaultTransformer(); }]);
     *
     * @param array $knownClassifiers The classifiers that this method
     * recognises are keys in this array, in descending order of
     * permission level, with a closure object as value.
     *
     * @return mixed The value returned by the closure whose key
     * matches the classifier name of the current user's permission.
     * If the user has none of the named classifiers, there's a real
     * problem and an exception is thrown.
     *
     * Note that this does not necessarily have to return a
     * transformer object, this is just the most common use case.
     */
    public static function applyClassifierForMatchingPermissionScope($request, $knownClassifiers)
    {
        $user = Sentinel::getUser();
        $basicPermission = static::retrieveValue($request, 'appliedBasicPermission');
        if (!static::haveRememberedValue($request, 'applicablePermissionScopes')) {
            throw new InsecurityException('You tried to select a transformer matching a scope, before selecting a scope.  This makes no sense.');
        }

        $scopes = static::retrieveValue($request, 'applicablePermissionScopes');
        $scopesLowToHigh = array_reverse($scopes);

        return function($obj) use ($scopesLowToHigh, $basicPermission, $knownClassifiers, $user) {
            // TODO: Cache already-evaluated transformers?
            // This is completely retarded code and needs to be refactored.
            //
            // We use hasAnyAccess instead of hasAccess to make testing mockery simpler
            foreach($scopesLowToHigh as $scopeName => $scope) {

                if (!$scope['decider']($obj)) continue;

                foreach($knownClassifiers as $classifierName => $transformer) {
                    if ($user->hasAnyAccess(["{$basicPermission}.{$scopeName}:{$classifierName}"])) {
                        // This level isn't mandatory (or should it be?), so don't remember it
                        if (is_callable($transformer)) {
                            return $transformer($obj);
                        } else {
                            return $transformer->transform($obj);
                        }
                    }
                }
                throw new InsecurityException('User has permission/scope "'.$basicPermission.'.'.$scopeName.'", but none of the known transformer classifiers!');
            }
        };
    }
}
