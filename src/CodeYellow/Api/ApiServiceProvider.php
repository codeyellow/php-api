<?php
namespace CodeYellow\Api;

use Illuminate\Support\ServiceProvider;
use CodeYellow\Api\Fractal\Serializer\BackboneSerializer;
use League\Fractal\Manager;
use Illuminate\Validation\DatabasePresenceVerifier;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $this->app->bind('League\Fractal\Manager', function () {
            $manager = new Manager();
            $manager->setSerializer(new BackboneSerializer());

            return $manager;
        });

        $this->app->bind('Illuminate\Validation\PresenceVerifierInterface', function () {
            return new DatabasePresenceVerifier($this->app['db']);
        });
    }
}
