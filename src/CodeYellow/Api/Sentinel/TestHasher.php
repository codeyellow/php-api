<?php
namespace CodeYellow\Api\Sentinel;

use \Cartalyst\Sentinel\Hashing\NativeHasher;


/**
 * Simple and fast (but NOT SECURE) hasher for fast unittesting
 */
class TestHasher extends NativeHasher
{
    public function hash($value)
    {
        return $value;
    }

    public function check($value, $hashedValue)
    {
        return $value === $hashedValue;
    }
}