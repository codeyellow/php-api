<?php
namespace CodeYellow\Api\Sentinel;

use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Cartalyst\Sentinel\Reminders\ReminderRepositoryInterface;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Users\UserRepositoryInterface;
use Cartalyst\Support\Traits\RepositoryTrait;
use Carbon\Carbon;


class TestReminderRepository implements ReminderRepositoryInterface
{
    use RepositoryTrait;
    /**
     * The user repository.
     *
     * @var \Cartalyst\Sentinel\Users\UserRepositoryInterface
     */
    protected $users;
    /**
     * The Eloquent reminder model name.
     *
     * @var string
     */
    protected $model = 'Cartalyst\Sentinel\Reminders\EloquentReminder';
    /**
     * The expiration time in seconds.
     *
     * @var int
     */
    protected $expires = 259200;

    /**
     * @var EloquentReminder[]
     */
    protected $reminders;

    /**
     * Create a new Illuminate reminder repository.
     *
     * @param  \Cartalyst\Sentinel\Users\UserRepositoryInterface  $users
     * @param  string  $model
     * @param  int  $expires
     * @return void
     */
    public function __construct(UserRepositoryInterface $users, $model = null, $expires = null)
    {
        $this->users = $users;
        if (isset($model)) {
            $this->model = $model;
        }
        if (isset($expires)) {
            $this->expires = $expires;
        }
    }


    /**
     * {@inheritDoc}
     */
    public function create(UserInterface $user)
    {
        $reminder = $this->createModel();
        $code = $this->generateReminderCode();
        $reminder->fill([
            'code'      => $code,
            'completed' => false,
        ]);
        $reminder->user_id = $user->getUserId();
        $reminder->created_at = Carbon::now();

        $this->reminders[$user->getUserId()][] = $reminder;

        return $reminder;
    }

    /**
     * {@inheritDoc}
     */
    public function exists(UserInterface $user, $code = null)
    {
        $expires = $this->expires();

        if (!isset($this->reminders[$user->getUserId()])) {
            return false;
        }

        foreach ($this->reminders[$user->getUserId()] as $reminder) {
            if ($reminder->created_at > $expires && $reminder->completed === false) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function complete(UserInterface $user, $code, $password)
    {
        $expires = $this->expires();

        $reminder = null;
        foreach ($this->reminders[$user->getUserId()] as $rem) {
            if ($rem->created_at > $expires && $rem->completed === false) {
                $reminder = $rem;
                break;
            }
        }


        if ($reminder === null) {
            return false;
        }
        $credentials = compact('password');
        $valid = $this->users->validForUpdate($user, $credentials);
        if ($valid === false) {
            return false;
        }

        $this->users->update($user, $credentials);
        $reminder->fill([
            'completed'    => true,
            'completed_at' => Carbon::now(),
        ]);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function removeExpired()
    {
        return true;
    }

    static $counter = 0;

    /**
     * Returns a random string for a reminder code.
     *
     * @return string
     */
    protected function generateReminderCode()
    {
        return 'correct' . (static::$counter++);
    }

    /**
     * Returns the expiration date.
     *
     * @return \Carbon\Carbon
     */
    protected function expires()
    {
        return Carbon::now()->subSeconds($this->expires);
    }
}
