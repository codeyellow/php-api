<?php
namespace CodeYellow\Api\Sentinel;

use Cartalyst\Sentinel\Activations\ActivationRepositoryInterface;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Traits\RepositoryTrait;
use Carbon\Carbon;


class TestActivationRepository implements ActivationRepositoryInterface
{
    use RepositoryTrait;

    /**
     * The Eloquent activation model name.
     *
     * @var string
     */
    protected $model = 'Cartalyst\Sentinel\Activations\EloquentActivation';

    /**
     * @var array of non completed activations
     */
    protected $nonCompletedActivations = [];

    /**
     * @var array of completed activations
     */
    protected $completedActivations = [];

    /**
     * To make it compatible with Illuminate Activation Repository
     *
     * @param  string  $model
     * @param  int  $expires
     * @return void
     */
    public function __construct($model = null, $expires = null)
    {
        $this->model = $model;

        $activation = $this->createModel();
        $code = $this->generateActivationCode();
        $activation->fill(compact('code'));
        $activation->user_id = 1;

        $this->completedActivations[1] = $activation;
    }

    /**
     * {@inheritDoc}
     */
    public function create(UserInterface $user)
    {
        $activation = $this->createModel();
        $code = $this->generateActivationCode();
        $activation->fill(compact('code'));
        $activation->user_id = $user->getUserId();

        $this->nonCompletedActivations[$user->getUserId()] = $activation;

        return $activation;
    }

    /**
     * {@inheritDoc}
     */
    public function exists(UserInterface $user, $code = null)
    {
        if (!isset($this->nonCompletedActivations[$user->getUserId()])) {
            return false;
        }

        if ($this->nonCompletedActivations[$user->getUserId()]->getCode() !== $code && !is_null($code)) {
            return false;
        }

        return $this->nonCompletedActivations[$user->getUserId()];
    }

    /**
     * {@inheritDoc}
     */
    public function complete(UserInterface $user, $code)
    {

        if (!isset($this->nonCompletedActivations[$user->getUserId()])) {
            return false;
        }

        $activation = $this->nonCompletedActivations[$user->getUserId()];
        $activation->fill([
            'completed'    => true,
            'completed_at' => Carbon::now(),
        ]);
        $this->completedActivations[$user->getUserId()] = $activation;
        unset($this->nonCompletedActivations);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function completed(UserInterface $user)
    {
        if (!isset($this->completedActivations[$user->getUserId()])) {
            return false;
        }

        return $this->completedActivations[$user->getUserId()];
    }

    /**
     * {@inheritDoc}
     */
    public function remove(UserInterface $user)
    {
        if (!isset($this->completedActivations[$user->getUserId()])) {
            return false;
        }

        unset($this->completedActivations[$user->getUserId()]);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function removeExpired()
    {
        return true;
    }

    static $counter = 0;
    /**
     * @return string Return the correct activation code for users
     */
    public function generateActivationCode()
    {
        return 'correct' . (static::$counter++);
    }
}