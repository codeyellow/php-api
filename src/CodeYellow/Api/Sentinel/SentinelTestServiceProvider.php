<?php

namespace CodeYellow\Api\Sentinel;

use \Illuminate\Support\ServiceProvider;
use Cartalyst\Sentinel\Sentinel;

/**
 * Nasty hack required so we don't have to bootstrap the entire
 * framework including a database, session store and whatnot, just to
 * be able to run the tests for the Middleware which uses Sentinel.
 */
class SentinelTestServiceProvider extends ServiceProvider
{
    static $persistences = null;
    static $users = null;
    static $roles = null;
    static $activations = null;
    static $dispatcher = null;

    public function register()
    {
        // Evil code stolen from the Sentinel test suite
        $this->app->singleton('sentinel', function ($app) {
                return new Sentinel(
                    static::$persistences = \Mockery::mock('Cartalyst\Sentinel\Persistences\IlluminatePersistenceRepository'),
                    static::$users        = \Mockery::mock('Cartalyst\Sentinel\Users\IlluminateUserRepository'),
                    static::$roles        = \Mockery::mock('Cartalyst\Sentinel\Roles\IlluminateRoleRepository'),
                    static::$activations  = \Mockery::mock('Cartalyst\Sentinel\Activations\IlluminateActivationRepository'),
                    static::$dispatcher   = \Mockery::mock('Illuminate\Events\Dispatcher')
                );
         });
    }
}
