<?php
namespace CodeYellow\Api\Illuminate\Mail;

use Illuminate\Support\Facades\File;

class Mailer extends \Illuminate\Mail\Mailer
{
    /**
     * Send a Swift Message instance.
     * Will log the file in development,
     * and send to predefind email in staging
     *
     * @param  \Swift_Message  $message
     * @return void
     */
    protected function sendSwiftMessage($message)
    {
        $this->environment  = \App::environment();

        switch ($this->environment) {
            // In production, just send the mail like
            // we would normally
            case 'production':
                return parent::sendSwiftMessage($message);
                break;

            // In staging, send the mail to predefined email
            case 'staging':
                // clear all recipients
                // and only add the predefined message
                $to = config('mail.staging_to');

                if (is_null($to)) {
                    throw new \Exception('mail.staging_to is not configured');
                }

                $message->setCc([]);
                $message->setBcc([]);
                $message->setTo($to);
                return parent::sendSwiftMessage($message);
                break;

            // In development, just log the file
            case 'development':
                $filesystem = app()->make(\Illuminate\Filesystem\Filesystem::class);
                $path = storage_path() . '/logs/mail';

                if (! $filesystem->isWritable($path)) {
                    throw new \Exception($path . ' is not writeable');
                }

                $file = $path . '/'.  microtime(true) . '.eml';

                $filesystem->put($file, $message->toString());
                break;
        }
    }
}
