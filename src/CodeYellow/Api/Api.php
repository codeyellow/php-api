<?php
namespace CodeYellow\Api;

use Response;
use Input;
use Symfony\Component\Yaml\Dumper as YamlDumper;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Pagination\PaginatorInterface;
use CodeYellow\Api\Fractal\Resource\Collection;
use CodeYellow\Api\Validation\Transformer;
use CodeYellow\Api\Validation\Exception as ValidationException;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use CodeYellow\Api\Middleware\ScopedAccess;

class Api extends Controller
{
    /**
     * Fractal manager.
     *
     * @var Manager
     */
    protected $fractal = null;

    /**
     * Input:: fascade
     */
    protected $input = null;

    /**
     * Response:: fascade
     */
    protected $response = null;

    protected $statusCode = 200;

    const CODE_WRONG_ARGS = 'ParameterError';
    const CODE_NOT_FOUND = 'ResourceNotFound';
    const CODE_INTERNAL_ERROR = 'InternalError';
    const CODE_UNAUTHORIZED = 'AuthenticationFailed';
    const CODE_FORBIDDEN = 'InsufficientAccountPermissions';
    const CODE_CSRF = 'CsrfTokenInvalid';
    const CODE_INVALID_MIME_TYPE = 'InvalidInput';
    const CODE_METHOD_NOT_ALLOWED = 'MethodNotAllowed';


    /**
     * Construct a new Api
     *
     * @param \League\Fractal\Manager fractalmanager.
     * @param \Illuminate\Http\Request Input handler.
     * @param \Illuminate\Contracts\Routing\ResponseFactory reponseFactory.
     */
    public function __construct(
        Manager $manager,
        Request $request,
        ResponseFactory $response
    ) {
        $this->fractal = $manager;
        $this->input = $request;
        $this->response = $response;

        $this->binder = $request->get('binder', false);

    }

    /**
     * Get the current scope for which this operation needs to be
     * applied, and apply the matching closure.
     *
     * Example: $collection = $this->scoped(['all' => function() use ($this) { return $this->service->findAll(); }]);
     *
     * This is just a convenience method for
     * CodeYellow\Api\Middleware\ScopedAccess::applyScopeForMatchingPermission($this->request, $scopes);
     *
     * @param array $knownScopes The scopes that this method
     * recognises are keys in this array, in descending order of
     * permission level, with a closure object as value.
     *
     * @return mixed The value returned by the closure whose
     * key matches the scope name of the current user's permission.
     * If the user has none of the named scopes, there's a real
     * problem and an exception is thrown.
     */
    protected function scoped($scopes)
    {
        return ScopedAccess::applyScopeForMatchingPermission($this->input, $scopes);
    }

    /**
     * Get the transformation classifier which this operation should
     * return, and apply the matching closure.  Can only be used after
     * calling scoped(), because it needs to know which of the
     * available permission scopes has been applied.
     *
     * Example: $transformer = $this->transformed(['full' => function() { return new DefaultTransformer(); }]);
     *
     * This is just a convenience method for
     * CodeYellow\Api\Middleware\ScopedAccess::applyClassifierForMatchingPermissionScope($this->request, $classifiers);
     *
     * @param array $knownClassifiers The classifiers that this method
     * recognises are keys in this array, in descending order of
     * permission level, with a closure object as value.
     *
     * @return mixed The value returned by the closure whose key
     * matches the classifier name of the current user's permission.
     * If the user has none of the named classifiers, there's a real
     * problem and an exception is thrown.
     *
     * Note that this does not necessarily have to return a
     * transformer object, this is just the most common use case.
     */
    protected function transformed($classifiers)
    {
        return ScopedAccess::applyClassifierForMatchingPermissionScope($this->input, $classifiers);
    }

    /**
     * Getter for statusCode.
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode.
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Respond with a new Item
     *
     * @param mixed           $item     The Item to respond with.
     * @param callable|string $callback Transformer.
     * @param string          $includes Standard includes if no other includes are set.
     */
    public function respondWithItem($item, $callback, $includes = null)
    {
        $resource = new Item($item, $callback);

        $rootScope = $this->fractal->createData($resource);

        $includes = $this->input->get('include') ?: $includes;
        if (!is_null($includes)) {
            $this->fractal->parseIncludes($includes);
        }

        if ($this->binder) {
            return $this->respondWithArray(['data' => $rootScope->toArray()]);
        } else {
            return $this->respondWithArray($rootScope->toArray());
        }
    }

    /**
     * Respond with a Collection
     *
     * @param mixed           $collection The Collection to respond with.
     * @param callable|string $callback   Transformer.
     * @param string          $includes   Standard includes if no other includes are set.
     */
    public function respondWithCollection($collection, $callback, $includes = null)
    {
        $resource = new Collection($collection, $callback);

        $rootScope = $this->fractal->createData($resource);

        $includes = $this->input->get('include') ?: $includes;
        if ($includes) {
            $this->fractal->parseIncludes($includes);
        }

        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Respond with a paginator
     *
     * @param \League\Fractal\Pagination\PaginatorInterface $paginator
     * @param callable|string $callback   Transformer.
     * @param string          $includes   Standard includes if no other includes are set.
     */
    public function respondWithPaginator(PaginatorInterface $paginator, $callback, $includes = null)
    {
        $resource = new Collection($paginator->getPaginator()->getCollection(), $callback);
        $resource->setPaginator($paginator);

        $rootScope = $this->fractal->createData($resource);

        $includes = $this->input->get('include') ?: $includes;
        if ($includes) {
            $this->fractal->parseIncludes($includes);
        }



        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Respond with an array
     *
     * @param array $array   Array to respond with.
     * @param array $headers The headers to attach to the array response.
     */
    public function respondWithArray(array $array, array $headers = [])
    {
        $mimeTypeRaw = $this->input->server('HTTP_ACCEPT', '*/*');

        // If its empty or has */* then default to JSON
        if ($mimeTypeRaw === '*/*') {
            $mimeType = 'application/json';
        } else {
            // You will probably want to do something intelligent with charset if provided.
            // This chapter just assumes UTF8 everything everywhere.
            $mimeParts = (array) explode(';', $mimeTypeRaw);
            $mimeType = strtolower($mimeParts[0]);
        }

        switch ($mimeType) {
            case 'application/x-yaml':
                $contentType = 'application/x-yaml';
                $dumper = new YamlDumper();
                $content = $dumper->dump($array, 2);
                break;
            default:
                // Just always output json if not recognized.
                $contentType = 'application/json';
                $content = json_encode($array);
                break;
        }


        $response = $this->response->make($content, $this->statusCode, $headers);
        $response->header('Content-Type', $contentType);



        return $response;
    }

    /**
     * Respond with an error code
     *
     * @param string $message          Error message to be displayed.
     * @param int    $errorCode        The error code that is returned.
     * @param array  $validationErrors Validation errors that may have occured.
     * @throws InvalidArgumentException if the given parameters are not logical.
     * @return Response
     */
    public function respondWithError($message, $errorCode, array $validationErrors = [])
    {
        if ($this->statusCode === 200) {
            throw new \InvalidArgumentException('It is not possible to error on a 200 code.');
        }



        $error = [
            'code' => $errorCode,
            'http_code' => $this->statusCode,
            'message' => $message,
        ];

        !empty($validationErrors) && $error['validation_errors'] = $validationErrors;

        return $this->respondWithArray([
            'error' => $error,
        ]);
    }

    /**
     * Notify the user that they provided an invalid CSRF token
     * @param string $message
     * @return Response
     */
    public function errorCsrf(
        $message = 'An invalid CSRF token was provided. Please provide a new token'
    ) {
        return $this->setStatuscode(403)->respondWithError($message, self::CODE_CSRF);
    }

    /**
     * Generates a Response with a 403 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorForbidden(
        $message = 'The account being accessed does not have sufficient permissions to execute this operation.'
    ) {
        return $this->setStatusCode(403)->respondWithError($message, self::CODE_FORBIDDEN);
    }

    /**
     * Generates a Response with a 500 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorInternalError($message = 'The server encountered an internal error. Please retry the request.')
    {
        return $this->setStatusCode(500)->respondWithError($message, self::CODE_INTERNAL_ERROR);
    }

    /**
     * Generates a Response with a 404 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorNotFound($message = 'The specified resource does not exist.')
    {
        return $this->setStatusCode(404)->respondWithError($message, self::CODE_NOT_FOUND);
    }

    /**
     * Generates a Response with a 405 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorMethodNotAllowed($message = 'This HTTP method is not allowed for this resource')
    {
        return $this->setStatusCode(405)->respondWithError($message, self::CODE_METHOD_NOT_ALLOWED);
    }

    /**
     * Generates a Response with a 401 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorUnauthorized($message = 'Server failed to authenticate the request.')
    {
        return $this->setStatusCode(401)->respondWithError($message, self::CODE_UNAUTHORIZED);
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return Response
     */
    public function errorWrongArgs(ValidationException $exception)
    {
        $transformer = new Transformer();

        return $this->setStatusCode(400)
            ->respondWithError(
                $exception->getMessage(),
                self::CODE_WRONG_ARGS,
                $transformer->transform($exception->getValidator()->getMessageBag()->getMessages())
            );
    }
}
